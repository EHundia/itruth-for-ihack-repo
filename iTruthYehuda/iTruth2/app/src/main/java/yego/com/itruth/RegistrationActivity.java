package yego.com.itruth;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.rey.material.widget.ListView;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;

import yego.com.itruth.R;


public class RegistrationActivity extends AppCompatActivity {

    private Socket mSocket;
    private static final String SERVER_ADDRESS = "http://10.0.1.4:8080";
    private MaterialEditText firstNameEditText;
    private MaterialEditText lastNameEditText;
    private MaterialEditText mailNameEditText;
    private MaterialEditText ip;
    private MaterialEditText passNameEditText;

    {
        try {
            mSocket = IO.socket(SERVER_ADDRESS);
        } catch (URISyntaxException e) {}
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);


        firstNameEditText = (MaterialEditText) findViewById(R.id.firstname);
        lastNameEditText = (MaterialEditText) findViewById(R.id.lastname);
        mailNameEditText = (MaterialEditText) findViewById(R.id.email);
        passNameEditText = (MaterialEditText) findViewById(R.id.password);
        ip = (MaterialEditText) findViewById(R.id.ip);
//        lv = (ListView) findViewById(R.id.listView);


//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
//                android.R.layout.simple_dropdown_item_1line, GENDERS);
//        BetterSpinner textView = (BetterSpinner)
//                findViewById(R.id.gender_spinner);
//        textView.setAdapter(adapter);

    }


    private static final String[] GENDERS = new String[] {
            "Male", "Female"
    };

    public void ConnectServer(View v) {
        try {
            mSocket = IO.socket(String.valueOf(ip.getText()));
        } catch (URISyntaxException e) {}

        mSocket.connect();
    }

    public void onClickBirthday(View v) {
        JSONObject object = new JSONObject();

        try {
            object.put("email", mailNameEditText.getText());
            object.put("firstname", firstNameEditText.getText());
            object.put("lastname", lastNameEditText.getText());
            object.put("pass", passNameEditText.getText());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        mSocket.emit("register", object);

        Log.d("onClickBirthday", object.toString());
    }
}
