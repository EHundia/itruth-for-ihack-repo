package yego.com.itruth.socketio.icd.login.request;

/**
 * Created by elihundia on 09/08/16.
 */
public class LoginRequestToServer {

    public String email;
    public String pass;

    public LoginRequestToServer(String email, String pass) {
        this.email = email;
        this.pass = pass;
    }

    @Override
    public String toString() {
        return "RegisterRequestToServer{" +
                "email='" + email + '\'' +
                ", pass='" + pass + '\'' +
                '}';
    }
}
