package yego.com.itruth.socketio.icd.registration.request;

/**
 * Created by elihundia on 09/08/16.
 */
public class RegisterRequestToServer {

    public String email;
    public String firstname;
    public String lastname;
    public String pass;
    public String language;
    public String gender;
    public String birthdate;
    public String country;

    public RegisterRequestToServer(String email, String firstname, String lastname, String pass, String language, String gender, String birthdate, String country) {
        this.email = email;
        this.firstname = firstname;
        this.lastname = lastname;
        this.pass = pass;
        this.language = language;
        this.gender = gender;
        this.birthdate = birthdate;
        this.country = country;
    }

    @Override
    public String toString() {
        return "RegisterRequestToServer{" +
                "email='" + email + '\'' +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", pass='" + pass + '\'' +
                ", language='" + language + '\'' +
                ", gender='" + gender + '\'' +
                ", birthdate='" + birthdate + '\'' +
                ", country='" + country + '\'' +
                '}';
    }
}
