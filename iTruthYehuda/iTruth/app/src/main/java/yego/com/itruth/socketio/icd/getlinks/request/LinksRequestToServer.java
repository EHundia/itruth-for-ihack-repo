package yego.com.itruth.socketio.icd.getlinks.request;

/**
 * Created by elihundia on 09/08/16.
 */
public class LinksRequestToServer {

    //  User email
    public String userEmail;

    //  Does the posts you want are positive or negative
    public boolean requestNegative = true;

    /**
     *
     * @param userEmail User email
     * @param requestNegative Does the posts you want are positive or negative
     */
    public LinksRequestToServer(String userEmail, boolean requestNegative) {
        this.userEmail = userEmail;
        this.requestNegative = requestNegative;
    }

    @Override
    public String toString() {
        return "LinksRequestToServer{" +
                "userEmail='" + userEmail + '\'' +
                ", requestNegative=" + requestNegative +
                '}';
    }
}
