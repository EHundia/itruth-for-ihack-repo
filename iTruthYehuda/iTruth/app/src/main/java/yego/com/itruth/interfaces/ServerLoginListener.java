package yego.com.itruth.interfaces;

import yego.com.itruth.socketio.icd.login.response.ServerLoginResponse;

public interface ServerLoginListener {
    public void onLoginResponse(ServerLoginResponse serverLoginResponse);
}
