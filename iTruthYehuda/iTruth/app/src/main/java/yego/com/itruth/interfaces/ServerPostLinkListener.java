package yego.com.itruth.interfaces;

import yego.com.itruth.socketio.icd.postlinks.response.ServerPostLinkResponse;

public interface ServerPostLinkListener {
    public void onPostLinkReponse(ServerPostLinkResponse serverPostLinkResponse
    );
}
