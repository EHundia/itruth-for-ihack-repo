package yego.com.itruth.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import yego.com.itruth.R;
import yego.com.itruth.gui.PostItem;

public class PostAdapter extends BaseAdapter {

    Context mContext;
    List<PostItem> mPostItems;

    public PostAdapter(Context context, List<PostItem> mPostItems) {
        this.mContext = context;
        this.mPostItems = mPostItems;
    }

    @Override
    public int getCount() {
        return mPostItems.size();
    }

    @Override
    public Object getItem(int position) {
        return mPostItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return mPostItems.indexOf(getItem(position));
    }

    /* private view holder class */
    private class ViewHolder {
        ImageView mPostImage;
        TextView mPostName;
        TextView mPostStatus;
        TextView mPostInfo;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;

        LayoutInflater mInflater = (LayoutInflater) mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.list_view_post_item, null);

            holder = new ViewHolder();
            holder.mPostName = (TextView) convertView.findViewById(R.id.post_name);
            holder.mPostImage = (ImageView) convertView.findViewById(R.id.post_image);
            holder.mPostStatus = (TextView) convertView.findViewById(R.id.post_status);
            holder.mPostInfo = (TextView) convertView.findViewById(R.id.post_info);
            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        PostItem postItem = mPostItems.get(position);

        holder.mPostImage.setImageResource(postItem.getmPostImageId());
        holder.mPostName.setText(postItem.getPostName());
        holder.mPostStatus.setText(postItem.getPostStatus());
        holder.mPostInfo.setText(postItem.getPostInfo());

        return convertView;
    }
}