package yego.com.itruth.socketio.server;

import android.util.Log;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONObject;

import java.net.URISyntaxException;

import yego.com.itruth.interfaces.ServerLoginListener;
import yego.com.itruth.interfaces.ServerPostLinkListener;
import yego.com.itruth.interfaces.ServerPostListReceivedListener;
import yego.com.itruth.interfaces.ServerRegisterListener;
import yego.com.itruth.socketio.icd.getlinks.response.LinkListJasonParserToJava;
import yego.com.itruth.socketio.icd.login.response.LoginResponseJasonParserToJava;
import yego.com.itruth.interfaces.ServerAPI;
import yego.com.itruth.socketio.icd.getlinks.request.LinkRequestJavaToJasonParser;
import yego.com.itruth.socketio.icd.getlinks.request.LinksRequestToServer;
import yego.com.itruth.socketio.icd.login.request.LoginRequestJavaToJason;
import yego.com.itruth.socketio.icd.login.request.LoginRequestToServer;
import yego.com.itruth.socketio.icd.postlinks.request.PostLink;
import yego.com.itruth.socketio.icd.postlinks.request.PostLinkJavaToJasonParser;
import yego.com.itruth.socketio.icd.postlinks.response.PostLinkResponseJasonParserToJava;
import yego.com.itruth.socketio.icd.registration.request.RegisterRequestJavaToJason;
import yego.com.itruth.socketio.icd.registration.request.RegisterRequestToServer;
import yego.com.itruth.socketio.icd.registration.response.RegisterResponseJasonParserToJava;

public class SocketIoService implements ServerAPI {

    private Socket mSocket;

    private ServerLoginListener serverLoginListener;
    private ServerRegisterListener serverRegisterListener;
    private ServerPostLinkListener serverPostLinkListener;
    private ServerPostListReceivedListener serverPostListReceivedListener;

    private static SocketIoService socketIoService = null;

    private SocketIoService() {}

    public static SocketIoService instance() {
        if (socketIoService == null) {
            socketIoService = new SocketIoService();
        }

        return socketIoService;
    }

    public void setServerPostLinkListener(ServerPostLinkListener serverPostLinkListener) {
        this.serverPostLinkListener = serverPostLinkListener;
    }

    public void setServerPostListReceivedListener(ServerPostListReceivedListener serverPostListReceivedListener) {
        this.serverPostListReceivedListener = serverPostListReceivedListener;
    }

    public void setServerRegisterListener(ServerRegisterListener serverRegisterListener) {
        this.serverRegisterListener = serverRegisterListener;
    }

    public void setServerLoginListener(ServerLoginListener serverLoginListener) {
        this.serverLoginListener = serverLoginListener;
    }

    //  POST TOPICS
    private static final String TOPIC_REGISTER = "register";
    private static final String TOPIC_LOGIN = "login";
    private static final String TOPIC_GETLINKS  = "getlinks";
    private static final String TOPIC_POSTLINK  = "postlink";

    //  EVENT TOPICS
    private static final String TOPIC_REGISTER_RESPONSE = "register_response";
    private static final String TOPIC_LOGIN_RESPONSE = "login_response";
    private static final String TOPIC_GETLINKS_RESPONSE  = "getlinks_response";
    private static final String TOPIC_POSTLINK_RESPONSE  = "postlink_response";

    //  To server jason builders
    private LinkRequestJavaToJasonParser linkRequestParser = new LinkRequestJavaToJasonParser();
    private LoginRequestJavaToJason loginRequestParser = new LoginRequestJavaToJason();
    private PostLinkJavaToJasonParser postLinkRequestParser = new PostLinkJavaToJasonParser();
    private RegisterRequestJavaToJason registerRequestParser = new RegisterRequestJavaToJason();

    //  From server java builders
    private RegisterResponseJasonParserToJava registerResponseBuilder = new RegisterResponseJasonParserToJava();
    private PostLinkResponseJasonParserToJava postLinkResponseBuilder = new PostLinkResponseJasonParserToJava();
    private LoginResponseJasonParserToJava loginResponseBuilder = new LoginResponseJasonParserToJava();
    private LinkListJasonParserToJava getLinksResponseBuilder = new LinkListJasonParserToJava();


    // ------------- SOCKET IO CALLBACKS ------------
    private Emitter.Listener onRegisterResponse = new Emitter.Listener() {
        @Override
        public void call(Object... args) {

            //  Notify Listener
            serverRegisterListener.onRegisterReponse(registerResponseBuilder.parseJason((JSONObject)args[0]));
        }
    };

    private Emitter.Listener onLoginResponse = new Emitter.Listener() {
        @Override
        public void call(Object... args) {

            //  Notify Listener
            serverLoginListener.onLoginResponse(loginResponseBuilder.parseJason((JSONObject)args[0]));
        }
    };

    private Emitter.Listener onPostLinkResponse = new Emitter.Listener() {
        @Override
        public void call(Object... args) {

            //  Notify Listener
            serverPostLinkListener.onPostLinkReponse(postLinkResponseBuilder.parseJason((JSONObject)args[0]));
        }
    };

    private Emitter.Listener onLinkRequestResponse = new Emitter.Listener() {
        @Override
        public void call(Object... args) {

            //  Notify Listener
            serverPostListReceivedListener.onPostLinksReceived(getLinksResponseBuilder.parseJason((JSONObject)args[0]));
        }
    };

    @Override
    public boolean init(final String serverAddress, final String serverPort) {
        //  Build the Web address ip and initialize the socket
        final String combinedAddress = "http://" + serverAddress + ":" + serverPort;

        if (mSocket == null) {
            try {
                mSocket = IO.socket(combinedAddress);

                //  Register to events from server and set handlers
                mSocket.on(TOPIC_REGISTER_RESPONSE, onRegisterResponse);
                mSocket.on(TOPIC_POSTLINK_RESPONSE, onPostLinkResponse);
                mSocket.on(TOPIC_LOGIN_RESPONSE, onLoginResponse);
                mSocket.on(TOPIC_GETLINKS_RESPONSE, onLinkRequestResponse);

            } catch (URISyntaxException e) {
                Log.d("SOCKET IO ERROR: ", "Error initializing the IO socket");
                return false;
            }


            mSocket.connect();
            Log.d("SOCKET.IO", " SUCCESFULLY CONNECTED...!!!");
        }

        return true;
    }

    @Override
    public void sendRequestLinks(LinksRequestToServer request) {
        JSONObject jsonToSend = linkRequestParser.parseJason(request);
        mSocket.emit(TOPIC_GETLINKS,jsonToSend);
    }

    @Override
    public void sendRegisterRequest(RegisterRequestToServer request) {
        JSONObject jsonToSend = registerRequestParser.parseJason(request);
        mSocket.emit(TOPIC_REGISTER,jsonToSend);
    }

    @Override
    public void sendPostLinkRequest(PostLink postLinkToPost) {
        JSONObject jsonToSend = postLinkRequestParser.parseJason(postLinkToPost);
        mSocket.emit(TOPIC_POSTLINK,jsonToSend);
    }

    @Override
    public void sendLoginRequest(LoginRequestToServer request) {
        JSONObject jsonToSend = loginRequestParser.parseJason(request);
        mSocket.emit(TOPIC_LOGIN,jsonToSend);
    }
}
