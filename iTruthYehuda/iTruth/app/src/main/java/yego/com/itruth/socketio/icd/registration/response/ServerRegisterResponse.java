package yego.com.itruth.socketio.icd.registration.response;

/**
 * Created by elihundia on 09/08/16.
 */
public class ServerRegisterResponse {

    public boolean success = false;
    public String errorReason;


    public ServerRegisterResponse(boolean success, String errorReason) {
        this.success = success;
        this.errorReason = errorReason;
    }

    @Override
    public String toString() {
        return "ServerRegisterResponse{" +
                "success=" + success +
                ", errorReason='" + errorReason + '\'' +
                '}';
    }
}
