package yego.com.itruth.socketio.icd.login.response;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by elihundia on 09/08/16.
 */
public class LoginResponseJasonParserToJava {

    // JSON Node names
    private static final String TAG_IS_SUCCESS = "regok";
    private static final String TAG_ERR_REASON = "reason";

    ServerLoginResponse response;
    public ServerLoginResponse parseJason(JSONObject jsonObj) {
        try {

            boolean isSuccess = Boolean.parseBoolean(jsonObj.getString(TAG_IS_SUCCESS));
            String reason = jsonObj.getString(TAG_ERR_REASON);

            response = new ServerLoginResponse(
                    isSuccess
                    , reason);

            Log.d("Registration Response:", response.toString());
        } catch (JSONException e1) {
            e1.printStackTrace();
        }

        return response;
    }
}