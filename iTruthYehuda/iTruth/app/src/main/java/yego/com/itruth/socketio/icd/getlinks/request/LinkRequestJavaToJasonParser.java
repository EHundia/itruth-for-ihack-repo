package yego.com.itruth.socketio.icd.getlinks.request;

import org.json.JSONException;
import org.json.JSONObject;

import yego.com.itruth.socketio.icd.registration.request.RegisterRequestToServer;

/**
 * Created by elihundia on 09/08/16.
 */
public class LinkRequestJavaToJasonParser {

    // JSON Node names
    private static final String TAG_EMAIL = "email";

    public JSONObject parseJason(LinksRequestToServer request) {

        JSONObject jsonObj = new JSONObject();
        try {
            jsonObj.put(TAG_EMAIL, request.userEmail);


        } catch (JSONException e) {
            e.printStackTrace();
        }

//        Log.d("Sending Links Req:",jsonObj.toString());

        return jsonObj;
    }
}