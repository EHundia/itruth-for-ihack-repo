package yego.com.itruth.socketio.icd.getlinks.response;

/**
 * Created by elihundia on 09/08/16.
 */
public class PostLink {

    public int postId = 0;
    public String postSubject;
    public boolean isProIsrael = true;
    public String postLink;
    public String platform;
    public String sphere;
    public String date;
    public String language;
    public String country;
    public String userNotes;
    public String publisherEmail;
    public boolean isActive;

    public PostLink(int postId, String postSubject, boolean isProIsrael, String postLink, String platform, String sphere, String date, String language, String country, String userNotes, String publisherEmail, boolean isActive) {
        this.postId = postId;
        this.postSubject = postSubject;
        this.isProIsrael = isProIsrael;
        this.postLink = postLink;
        this.platform = platform;
        this.sphere = sphere;
        this.date = date;
        this.language = language;
        this.country = country;
        this.userNotes = userNotes;
        this.publisherEmail = publisherEmail;
        this.isActive = isActive;
    }

    @Override
    public String toString() {
        return "PostLink{" +
                "postId=" + postId +
                ", postSubject='" + postSubject + '\'' +
                ", isProIsrael=" + isProIsrael +
                ", postLink='" + postLink + '\'' +
                ", platform='" + platform + '\'' +
                ", sphere='" + sphere + '\'' +
                ", date='" + date + '\'' +
                ", language='" + language + '\'' +
                ", country='" + country + '\'' +
                ", userNotes='" + userNotes + '\'' +
                ", publisherEmail='" + publisherEmail + '\'' +
                ", isActive=" + isActive +
                '}';
    }
}
