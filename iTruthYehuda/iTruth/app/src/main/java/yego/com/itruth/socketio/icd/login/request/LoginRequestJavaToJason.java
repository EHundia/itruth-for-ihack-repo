package yego.com.itruth.socketio.icd.login.request;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by elihundia on 09/08/16.
 */
public class LoginRequestJavaToJason {

    // JSON Node names
    private static final String TAG_EMAIL = "email";
    private static final String TAG_PASSWORD = "pass";

    private LoginRequestToServer linkInfo;

    public JSONObject parseJason(LoginRequestToServer request) {

        JSONObject jsonObj = new JSONObject();
        try {
            jsonObj.put(TAG_EMAIL, request.email);
            jsonObj.put(TAG_PASSWORD, request.pass);

        } catch (JSONException e) {
            e.printStackTrace();
        }

//        Log.d("Sending Reg Req:",jsonObj.toString());

        return jsonObj;
    }
}