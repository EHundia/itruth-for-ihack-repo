package yego.com.itruth.interfaces;

import yego.com.itruth.socketio.icd.registration.response.ServerRegisterResponse;

public interface ServerRegisterListener {
    public void onRegisterReponse(ServerRegisterResponse serverRegisterResponse);
}
