package yego.com.itruth.socketio.icd.registration.request;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.HashMap;

/**
 * Created by elihundia on 09/08/16.
 */
public class RegisterRequestJavaToJason {

    // JSON Node names
    private static final String TAG_EMAIL = "email";
    private static final String TAG_FIRSTNAME = "firstname";
    private static final String TAG_LASTNAME = "lastname";
    private static final String TAG_PASSWORD = "pass";
    private static final String TAG_LANGUAGE = "language";
    private static final String TAG_GENDER = "gender";
    private static final String TAG_BIRTHDATE = "birthdate";
    private static final String TAG_COUNTRY = "country";

    private RegisterRequestToServer linkInfo;

    public JSONObject parseJason(RegisterRequestToServer request) {

        JSONObject jsonObj = new JSONObject();
        try {
            jsonObj.put(TAG_EMAIL, request.email);
            jsonObj.put(TAG_BIRTHDATE, request.birthdate);
            jsonObj.put(TAG_COUNTRY, request.country);
            jsonObj.put(TAG_FIRSTNAME, request.firstname);
            jsonObj.put(TAG_LASTNAME, request.lastname);
            jsonObj.put(TAG_GENDER, request.gender);
            jsonObj.put(TAG_PASSWORD, request.pass);
            jsonObj.put(TAG_LANGUAGE, request.language);

        } catch (JSONException e) {
            e.printStackTrace();
        }

//        Log.d("Sending Reg Req:",jsonObj.toString());

        return jsonObj;
    }
}