package yego.com.itruth.interfaces;

import yego.com.itruth.socketio.icd.getlinks.request.LinksRequestToServer;
import yego.com.itruth.socketio.icd.postlinks.request.PostLink;
import yego.com.itruth.socketio.icd.login.request.LoginRequestToServer;
import yego.com.itruth.socketio.icd.registration.request.RegisterRequestToServer;

/**
 * Created by elihundia on 09/08/16.
 */
public interface ServerAPI {

    /**
     * Expecting to receive the activity that listens on the events
     * @param listener The main activity that handles the events
     * @return
     */
    public boolean init(final String serverAddress, final String serverPort);

    /**
     * Notice that the links you request can be negative or positive ones
     * @param request
     */
    public void sendRequestLinks(LinksRequestToServer request);

    /**
     * Registration request
     * @param request
     */
    public void sendRegisterRequest(RegisterRequestToServer request);

    /**
     * PostLink to post
     * @param postLinkToPost
     */
    public void sendPostLinkRequest(PostLink postLinkToPost);

    /**
     *
     * @param request
     */
    public void sendLoginRequest(LoginRequestToServer request);

}
