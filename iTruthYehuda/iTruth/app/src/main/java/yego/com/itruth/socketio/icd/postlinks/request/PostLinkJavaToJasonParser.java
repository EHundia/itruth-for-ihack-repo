package yego.com.itruth.socketio.icd.postlinks.request;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by elihundia on 09/08/16.
 */
public class PostLinkJavaToJasonParser {

    // JSON Node names
    // JSON Node names
    private static final String TAG_POSTLIST = "postlist";
    private static final String TAG_POST_ID = "postid";
    private static final String TAG_POST_ITEM = "postitem";
    private static final String TAG_POST_SUBJECT = "subject";
    private static final String TAG_POST_ISPRO = "isPro";
    private static final String TAG_POST_LINK = "link";
    private static final String TAG_POST_PLATFORM = "platform";
    private static final String TAG_POST_SPHERE = "sphere";
    private static final String TAG_POST_DATE = "date";
    private static final String TAG_POST_LANGUAGE = "language";
    private static final String TAG_POST_COUNTRY = "country";
    private static final String TAG_POST_NOTE = "note";
    private static final String TAG_POST_PUBLISHER = "publisher";
    private static final String TAG_IS_ACTIVE = "isActive";

    public JSONObject parseJason(PostLink request) {

        JSONObject jsonObj = new JSONObject();

        try {

            //  Get post information
            jsonObj.put(TAG_POST_SUBJECT, request.postSubject);
            jsonObj.put(TAG_POST_ISPRO, request.isProIsrael);
            jsonObj.put(TAG_POST_LINK, request.postLink);
            jsonObj.put(TAG_POST_PLATFORM, request.platform);
            jsonObj.put(TAG_POST_SPHERE,request.sphere);
            jsonObj.put(TAG_POST_DATE, request.date);
            jsonObj.put(TAG_POST_COUNTRY, request.country);
            jsonObj.put(TAG_POST_LANGUAGE,request.language);
            jsonObj.put(TAG_POST_NOTE, request.userNotes);
            jsonObj.put(TAG_POST_PUBLISHER, request.publisherEmail);


        } catch (JSONException e) {
            e.printStackTrace();
        }

//        Log.d(Sending POST req:",jsonObj.toString());

        return jsonObj;
    }
}