package yego.com.itruth.interfaces;

import java.util.ArrayList;

import yego.com.itruth.socketio.icd.getlinks.response.PostLink;

public interface ServerPostListReceivedListener {
    public void onPostLinksReceived(ArrayList<PostLink> posts);
}
