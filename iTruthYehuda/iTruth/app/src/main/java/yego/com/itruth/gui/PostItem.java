package yego.com.itruth.gui;

public class PostItem {

    private String mPostName;
    private int mPostImageId;
    private String mPostStatus;
    private String mPostInfo;

    public PostItem(String postName, int postImageId, String postStatus, String postInfo) {

        this.mPostName = postName;
        this.mPostImageId = postImageId;
        this.mPostStatus = postStatus;
        this.mPostInfo = postInfo;
    }

    public String getPostName() {
        return mPostName;
    }

    public void setPostName(String postName) {
        this.mPostName = postName;
    }

    public int getmPostImageId() {
        return mPostImageId;
    }

    public void setPostImageId(int postImageId) {
        this.mPostImageId = postImageId;
    }

    public String getPostStatus() {
        return mPostStatus;
    }

    public void setPostStatus(String postStatus) {
        this.mPostStatus = postStatus;
    }

    public String getPostInfo() {
        return mPostInfo;
    }

    public void setPostInfo(String postInfo) {
        this.mPostInfo = postInfo;
    }
}