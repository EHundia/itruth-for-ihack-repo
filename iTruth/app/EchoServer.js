var io = require('socket.io')(3001);
console.log('Game server has started.. :-)');

io.sockets.on('connection', function(socket) {
  socket.on('echo', function(data) {
    socket.emit('echo back', data);
  });
});