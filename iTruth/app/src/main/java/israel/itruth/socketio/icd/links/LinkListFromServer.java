package israel.itruth.socketio.icd.links;

/**
 * Created by elihundia on 09/08/16.
 */
public class LinkListFromServer {

    public int postId = 0;
    public String postSubject;
    public boolean isProIsrael = true;
    public String postLink;
    public String platform;
    public String sphere;
    public String date;
    public String language;
    public String country;
    public String userNotes;
    public String publisherEmail;
    public boolean isActive;

}
