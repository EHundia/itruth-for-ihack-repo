package israel.itruth.socketio.icd.links;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by elihundia on 09/08/16.
 */
public class LinkListJasonParserToJava {

    // JSON Node names
    private static final String TAG_POSTLIST = "postlist";
    private static final String TAG_POST_ID = "postid";
    private static final String TAG_POST_ITEM = "postitem";
    private static final String TAG_POST_SUBJECT = "subject";
    private static final String TAG_POST_ISPRO = "isPro";
    private static final String TAG_POST_LINK = "link";
    private static final String TAG_POST_PLATFORM = "platform";
    private static final String TAG_POST_SPHERE = "sphere";
    private static final String TAG_POST_DATE = "date";
    private static final String TAG_POST_LANGUAGE = "language";
    private static final String TAG_POST_COUNTRY = "country";
    private static final String TAG_POST_NOTE = "note";
    private static final String TAG_POST_PUBLISHER = "publisher";
    private static final String TAG_IS_ACTIVE = "isActive";

    //  Array list with a hash map to hold the post list
    ArrayList<HashMap<String, String>> postList = new ArrayList<HashMap<String, String>>();

    public void parseJason(JSONObject jsonObj) {
        try {

            JSONArray posts = null;

            // Getting JSON Array node
            posts = jsonObj.getJSONArray(TAG_POSTLIST);

            // looping through All Contacts
            for (int i = 0; i < posts.length(); i++) {
                JSONObject c = posts.getJSONObject(i);

                //  Get ID
                String id = c.getString(TAG_POST_ID);

                //  Get Is Active
                String isActive = c.getString(TAG_IS_ACTIVE);

                //  Get post information
                JSONObject post = c.getJSONObject(TAG_POST_ITEM);
                String subject = post.getString(TAG_POST_SUBJECT);
                String isPro = post.getString(TAG_POST_ISPRO);
                String link = post.getString(TAG_POST_LINK);
                String platform = post.getString(TAG_POST_PLATFORM);
                String sphere = post.getString(TAG_POST_SPHERE);
                String date = post.getString(TAG_POST_DATE);
                String country = post.getString(TAG_POST_COUNTRY);
                String language = post.getString(TAG_POST_LANGUAGE);
                String note = post.getString(TAG_POST_NOTE);
                String publisher = post.getString(TAG_POST_PUBLISHER);

                // tmp hashmap for single contact
                HashMap<String, String> contact = new HashMap<String, String>();

                // adding each child node to HashMap key => value
                contact.put(TAG_POST_ID, id);
                contact.put(TAG_IS_ACTIVE, isActive);
                contact.put(TAG_POST_SUBJECT, subject);
                contact.put(TAG_POST_ISPRO, isPro);
                contact.put(TAG_POST_LINK, link);
                contact.put(TAG_POST_PLATFORM, platform);
                contact.put(TAG_POST_SPHERE, sphere);
                contact.put(TAG_POST_DATE, date);
                contact.put(TAG_POST_COUNTRY, country);
                contact.put(TAG_POST_LANGUAGE, language);
                contact.put(TAG_POST_NOTE, note);
                contact.put(TAG_POST_PUBLISHER, publisher);

                Log.d("POST LIST: ", contact.toString());
                // adding contact to contact list
                postList.add(contact);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}