package israel.itruth.socketio.api;

import android.util.Log;

import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import java.net.URISyntaxException;

/**
 * Created by elihundia on 08/08/16.
 */
public class SocketIoService {

    private Socket mSocket;

    /**
     * Init the service socket
     * @param serverAddress Server address
     * @param serverPort Server socket
     */
    public void init(final String serverAddress, final String serverPort) {
        //  Build the Web address ip and initialize the socket
        final String combinedAddress = "http://" + serverAddress + ":" + serverPort;
        try {
            mSocket = IO.socket(combinedAddress);
        } catch (URISyntaxException e) {
            Log.d("SOCKET IO ERROR: ", "Error initializing the IO socket");
        }

    }
}
