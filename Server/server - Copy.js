// Establish a connection to the mongo database
var mongo = require('mongodb').MongoClient;

// Establish a connection from index.html to socket.io
var client = require('socket.io').listen(8080).sockets;

var resultOk = {'regok': true, 'reason': 'ok'};

/*  Start a connection to the mongo database and tell server.js where to find it.
    Wrap the connection between the client and server inside the connection to
    the database to make the datbase connection mandatory.r
*/
mongo.connect('mongodb://127.0.0.1/iTruth', function (err, db) {
    if (err) throw err;
    console.log("Server started successfully!, port 8080");


    client.on('connection', function (socket) {
        console.log("Client Joined Successfully!!!!");
        
        socket.on('register', function (data) {
            console.log("register:  RAW data: " + data);

            // registration data validation.
            if (data.firstname === '' || data.lastname === '' || data.email === '' ||
                data.birthdate === '' || data.pass === '' || data.country === '') {

                socket.emit('register_response', {'regok':false, 'reason':'validation failed, you missed one!'});
                console.log("register: "+data + " validation failed : JSON validity check");
                return;//fail_validation;
            }


            console.log("register: before validation of email in DB");
            // validate email not found in DB
            var users = db.collection('users');
            var userCount = users.count({email: data.email},function (err, count) {
                console.log("register: num of DB records :" + count);
                if (count > 0) {
                    // email already exist in DB
                    console.log("register: after validation of email in DB. email exists ");
                    socket.emit('register_response', {'regok': false, 'reason': 'email already exist'});
                    return;

                } else {
                    // reached here - passed validation, complete registration
                    console.log("register: after validation of email in DB. email is new");
                    users.insertOne(data, function (err, result) {
                        // assert.equal(err, null);
                        console.log("register: after saving to DB. ");
                        socket.emit('register_response', resultOk);
                        return;
                    });
                }
            });
        });


        socket.on('login', function (data) {

            console.log("login : email: "+data.email+", pass: "+data.pass);
            var userCount = db.collection('users').count({email: data.email, pass: data.pass},function (err, count) {
                console.log(count);
                if (count > 0) { // there is a proper record of this email and pass !
                    // email already exist in DB
                    console.log("login : email + pass validated ");
                    socket.emit('login_response', resultOk);
                    return;

                } else {
                    // reached here - validation failed (no proper record)
                    console.log("login : email + password FAILED!");
                    socket.emit('login_response', {'regok': false, 'reason': 'email or password incorrect'});
                    return;
                }
            });
        });


        socket.on('getlinks', function (data) {
            var email = data.email;
            console.log("getlinks : received by email: "+email);
            // var links = db.collection('links');
            // //TODO replace next line with logic function that finds best posts for the user
            // var stream = links.find().sort().limit(10).stream();
            // stream.on('data', function (links) {
            //     console.log('streaming best links for ' + email);
            //     socket.emit('getlinks_response', links.toString());
            //});
            socket.emit('getlinks_response', mockpostlist);
        });

        socket.on('postlink', function (data) {
            console.log("postlink : received by email: "+data.publisher);
            //TODO add logic to handle duplicate links
            var links = db.collection('links');
            links.insertOne(data, function (err, result) {
                if (err) {
                    console.log("postlink: error " + err);
                    socket.emit('postlink_response', {'regok':false, 'reason': err.toString()});
                    return;
                }

                console.log("postlink: saved to DB. ");
                socket.emit('postlink_response', resultOk);

                //TODO select proper users to push to

            });
        });

        var mockpostlist =
        {
            "postlist": [
                {
                    "postid": 234234,
                    "postitem": {
                        "subject": "subject",
                        "isPro": true,
                        "link": "link",
                        "platform": "facebook",
                        "sphere": "doctors group",
                        "date": "2012-04-23T18:25:43.511Z",
                        "language": "english",
                        "country": "country",
                        "note": "free text",
                        "publisher": "yehuda"
                    },
                    "isActive": true
                },
                {
                    "postid": 2342234,
                    "postitem": {
                        "subject": "subject",
                        "isPro": true,
                        "link": "link",
                        "platform": "facebook",
                        "sphere": "doctors group",
                        "date": "2012-04-23T18:25:43.511Z",
                        "language": "english",
                        "country": "country",
                        "note": "free text",
                        "publisher": "eli"
                    },
                    "isActive": true
                }
            ]

        };

        /* END OF THIRD CODE TUTORIAL */
//    });
    });
}); // end of mongodb conn