var os = require('os');
var should = require("chai").should();
var socketio_client = require('socket.io-client');

var end_point = 'http://' + os.hostname() + ':8080';
var opts = {forceNew: true};


describe("async test with socket.io", function () {
    this.timeout(10000);

    it('Response should be an object', function (done) {
        setTimeout(function () {
            var socket_client = socketio_client(end_point, opts);

            var userdata =  {
                "firstname": "first name",
                "lastname": "last name",
                "email": "email",
                "password": "password",
                "birthdate": "2012-04-23T18:25:43.511Z",
                "address": {
                    "country": "country",
                    "city": "city",
                    "street": "street"
                },
                "language": [
                    "english",
                    "hebrew"
                ],
                "gender": true,
                "ocupation": "ocupation",
                "publicsphere": [
                    "stanford collegue",
                    "doctors blog"
                ]
            };

            socket_client.emit('connection', userdata);

            socket_client.on('alert', function (data) {
                console.log("alert  = "+data);
                data.should.be.an('object');
                socket_client.disconnect();
                done();
            });

            socket_client.on('clear-login', function (data) {
                console.log(data);
                socket_client.disconnect();
                done();
            });
        }, 4000);
    });
});/**
 * Created by GALVI on 8/10/2016.
 */
