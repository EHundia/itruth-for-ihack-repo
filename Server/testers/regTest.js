var socket = require('socket.io-client').connect('http://localhost:8080');
console.log("started");

socket.on('connect', function () {
    console.log("socket connected");
});

socket.on('alert', function (data) {
    console.log("alert  = "+data);
    socket.disconnect();
});

socket.on('clear-login', function (data) {
    console.log(data);
    socket.disconnect();
});

    var userdata =  {
        "firstname": "first name",
        "lastname": "last name",
        "email": "email",
        "password": "password",
        "birthdate": "2012-04-23T18:25:43.511Z",
        "address": {
            "country": "country",
            "city": "city",
            "street": "street"
        },
        "language": [
            "english",
            "hebrew"
        ],
        "gender": true,
        "ocupation": "ocupation",
        "publicsphere": [
            "stanford collegue",
            "doctors blog"
        ]
    };

    socket.emit('register', userdata);
console.log("send message");

/* Created by GALVI on 8/10/2016.
 */
